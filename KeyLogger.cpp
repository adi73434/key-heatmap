
#define _WIN32_WINNT 0x0500
#include <Windows.h>
#include <iostream>
#include <vector>
#include <thread>
#include <mutex>

#include "Globals.h"

// Key presses in positions matching https://docs.microsoft.com/en-us/windows/win32/inputdev/virtual-key-codes
std::vector<int> keyPressCounts(numOfKeyCodes+1);
// This saves the keypresses in the same way as keyPressCounts, as opposed to the previous method of saving them chronologically
std::vector<int> keyPressCountsOverflow(numOfKeyCodes+1);

// -----------------------------------------------------------------------------
// keyLogger. Runs on separate thread.
// If currentlySaving == "true": log new key presses to tempKeyData
// If currentlySaving == "wasTrue": add tempKeyData to activeKeyData,
// currentlySaving = "false" If currentlySaving == "false": log new key presses
// to activeKeyData
// -----------------------------------------------------------------------------

const std::vector<int> &retrieve_values()
{
	return keyPressCounts;
}

void add_values_sync(std::vector<int> &vecToAddFrom)
{
	for (int i = 0; i < vecToAddFrom.size(); ++i)
	{
		keyPressCounts[i] = keyPressCounts[i] + vecToAddFrom[i];
	}
	// for (int i = 0; i < vecToAddFrom.size(); ++i)
	// {
	// overflowWhileSaving[i] = overflowWhileSaving[i] + vecToAddFrom[i];
	// }
}

void reset_values()
{
	for (int i = 0; i < keyPressCounts.size(); ++i)
		keyPressCounts[i] = 0;
}

void key_logger()
{

	// ShowWindow(GetConsoleWindow(), SW_HIDE);

	// IDK if this is redundant or what
	char KEY = 'x';

	std::vector<bool> pressedBtns(numOfKeyCodes+1);
	for (auto x : pressedBtns)
		x = false;

	while (true)
	{
		// 20 Clicks Per Second (CPS) equates to 50MS
		// Within each iteration, each individual key can only be pressed once.
		// So if the sleep was every second, you'd only be able to log 1 click per second per key,
		// but multiple different keys are loggable even if they're hit at "exactly" the same time
		// 20CPS (50ms) should be enough for typing https://typing-speed.net/ranking
		// I'm gonna use 31.25 clicks per second (32ms) just for shits and giggles
		Sleep(32);
		for (int KEY = 8; KEY <= numOfKeyCodes; KEY++)
		{
			if (GetAsyncKeyState(KEY) != 0) // Key pressed/held
			{
				pressedBtns[KEY] = true;
			}
			else if (pressedBtns[KEY] == true) // Key released. Only triggers once on release
			{
				pressedBtns[KEY] = false;
				switch (keylogOpMode)
				{

				case 0:
					// Normal operation
					keyPressCounts[KEY]++;
					// std::cout << KEY << ": " << keyPressCounts[KEY] << "\n";
					break;

				case 1:
					// Just finished saving/loading: Add from overflow to normal, reset overflow, and continue as normal
					for (int i = 0; i < keyPressCountsOverflow.size(); ++i)
					{
						keyPressCounts[i] += keyPressCountsOverflow[i];
						keyPressCountsOverflow[i] = 0;
					}
					keylogOpMode = 0;
					break;

				case 2:
					// Currently saving/loading: Add to overflow
					keyPressCountsOverflow[KEY]++;
					break;
				default:
					break;
				}
			}
		}
	}
}
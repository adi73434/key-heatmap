# key-heatmap

This is a simple program that logs key press counts. It uses a `tempFile.txt` for syncing to every half an hour, and to keep the state saved if the program is closed before a persistent `saveFile` is made.

Because I'm a dumb nerd, I didn't use any libraries for reading/writing; I somewhat went along with the INI `param=value` and `# comment` format, but it's fragile and "comments" aren't really comments.

- The first *"comment"* is the epoch at last save.
- The second *"comment"* is the epoch at last initialisation of the program.
- All *"values"* following the first two lines are [Microsoft Windows Virtual-Key Codes](https://docs.microsoft.com/en-us/windows/win32/inputdev/virtual-key-codes) which correspond to the key. i.e., `Q` is `0x51`, but it is saved as `81`. The value of these is the press count of that indidivual key.

Upon launch, the `tempFile.txt` is read.
Every 30 mins, the program checks conditions regarding `tempFile.txt`: If the currently running program is 30 mins ahead of when `tempFile.txt` was last synced, *and* the "start time of the program" of the current program and the tempFile match, the program saves to `tempFile.txt`

A `saveFile` with name format `saveFile %Y-%m-%d %H_%M_%S.txt` (i.e., `saveFile 2020-12-31 23_59_57.txt`) is saved once every day, with the first save being 24 hours after program launch. Upon saving, the current keycount and `tempFile` are reset so as to keep a log of "keypresses in a day".

The current implementation logs keys 8-222, as to avoid logging mouse key presses. I believe this covers what is needed, but you could just change this via the `numOfKeyCodes` int in `Globals.cpp`; if you only need 90 keys and... for some reason want to "reduce overhead", you can lower it.

Note that a hardware macro such as those with QMK Configurator will send the macro's corresponding virtual keycode; if you rebind `Fn` + `]` to `Ctrl` + `NumPlus`, Windows will see that as `Ctrl` + `NumPlus`. I assume stuff like Corisair/iCUE results in the same thing.

## Commands
`:quit` Quits
`:writeTemp` Saves current session to tempFile
`:saveDay` Saves current session to a new persistent day file
`:help` Shows the help menu within the console

## Plans
- I want to use a `config.ini` or-whatever file with some options for `tempFile` and `saveFile` save directories, saveFile date format, tempFile sync interval, saveFile save interval, and potentially an option to open in a hidden mode with keybinds to 
#define _WIN32_WINNT 0x0500
#include <Windows.h>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <chrono>
#include <thread>
#include <mutex>
#include <vector>
#include <map>
#include <algorithm>

#include "Globals.h"

#include "KeyLogger.h"

const int hourInMs = 3600000;
const int dayInMs = 86400000;

std::chrono::milliseconds get_ms_since_epoch()
{
	return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
}

std::chrono::minutes get_mins_since_epoch()
{
	return std::chrono::duration_cast<std::chrono::minutes>(std::chrono::system_clock::now().time_since_epoch());
}

std::string current_date_time()
{
	time_t timeNow = time(0);
	struct tm timeInfo;
	char buf[80];
	localtime_s(&timeInfo, &timeNow);
	strftime(buf, sizeof(buf), "%Y-%m-%d %H_%M_%S", &timeInfo);

	return buf;
}

// Template to allow ofstream/fstream
template <typename fileHandleType>
void load_logger_times(fileHandleType &fileHandle)
{
	std::cout << "load_logger_times\n";

	// std::cout << "1\n";

	if (fileHandle.good())
	{
		// std::cout << "2\n";
		std::string ltse;
		getline(fileHandle, ltse); // First line = Epoch at last save
		if (ltse.empty())
			lastTempSaveEpoch = 0;
		else
		{
			ltse.erase(std::remove_if(ltse.begin(), ltse.end(), isspace), ltse.end()); // Remove whitespace
			std::cout << ltse.substr(1) << "\n";
			lastTempSaveEpoch = std::stoll(ltse.substr(1)); // Remove first char "#" then convert to int
		}

		std::string ltie;
		getline(fileHandle, ltie); // Second line = Epoch at last initialisation of program
		if (ltie.empty())
			lastTempInitEpoch = 0;
		else
		{
			ltie.erase(std::remove_if(ltie.begin(), ltie.end(), isspace), ltie.end()); // Remove whitespace
			std::cout << ltie << "\n";
			lastTempInitEpoch = std::stoll(ltie.substr(1)); // Remove first char "#" then convert to int
		}
	}
	else
	{
		// std::cout << "3\n";
		lastTempSaveEpoch = 0;
		lastTempInitEpoch = 0;
	}
}

// Template to allow ofstream/fstream
template <typename fileHandleType>
void write_logger_times(fileHandleType &fileHandle, long long &currentEpoch)
{

	fileHandle << "# " << currentEpoch << "\n";
	fileHandle << "# " << activeInitEpoch << "\n";
	std::cout << "-write_logger_times\n";
}

// Template to allow ifstream/fstream
template <typename fileHandleType>
void load_keypress_count(fileHandleType &fileHandle, std::vector<int> &vecLoadInto)
{
	// std::cout << "load_keypress_count\n";

	std::map<int, int>;
	std::string line;
	// Load everything following a colon to a vector where the "name" indexes to match the ASCII virtual key code
	while (getline(fileHandle, line))
	{
		line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
		if (line[0] == '#' || line.empty())
			continue;
		auto delimiterPos = line.find("=");
		std::string name = line.substr(0, delimiterPos);
		std::string value = line.substr(delimiterPos + 1);
		// std::cout << "Reading In: " << name << " " << value << '\n';
		vecLoadInto[std::stoi(name)] = std::stoi(value);
	}
	// for (int i = 0; i < vecLoadInto.size(); ++i)
	// {
	// std::cout << "As Loaded: " << vecLoadInto[i] << "\n";
	// }
	std::cout << "-load_keypress_count\n";
}

// Template to allow ofstream/fstream
template <typename fileHandleType>
void write_keypress_count(fileHandleType &fileHandle, const std::vector<int> &vecToSave)
{
	// std::cout << "write_keypress_count\n";

	for (int i = 0; i < vecToSave.size(); ++i)
	{
		// std::cout << "Writing: " << vecToSave[i] << "\n";
		fileHandle << i << "=" << vecToSave[i] << "\n";
	}
	std::cout << "-write_keypress_count\n";
}

void sync_temp_file()
{
	std::cout << "sync_temp_file\n";
	// If daily saveFile is currently being saved to disk, don't attempt to sync
	if (savingPersistentFile)
		return;

	// std::cout << "sync_temp_file\n";

	std::ifstream rTemp;
	rTemp.open("tempFile.txt");

	load_logger_times(rTemp);

	long long currentEpoch = get_ms_since_epoch().count();
	// int minsSinceLastSave = (currentEpoch/60000) - (lastTempSaveTime / 60000);
	// std::cout << "activeInitEpoch: " << activeInitEpoch << "\n";
	// std::cout << "lastTempInitEpoch: " << lastTempInitEpoch << "\n";
	// std::cout << "currentEpoch: " << currentEpoch << "\n";
	// std::cout << "lastTempSaveEpoch: " << lastTempSaveEpoch << "\n";

	// If a lastTempInitEpoch was set, i.e., program was running, and the active init epoch is of a later date/time
	// then assume the program was closed and load temp file data and resync time
	if (activeInitEpoch > lastTempInitEpoch)
	{
		std::cout << "sync_temp_file: restoring tempFile\n";

		// NOTE: Important to ensure keylogger switches variables so that retrieve_values and merging is accurate
		keylogOpMode = 2;

		std::vector<int> loadedKeyPressCounts(numOfKeyCodes+1);

		// If the file exists, try to load keypresses
		if (rTemp.good())
		{
			// std::cout << "tempFile exists, loading\n";

			// std::cout << "failed to open\nassuming not existing\n";
			load_keypress_count<std::ifstream>(rTemp, loadedKeyPressCounts);
		}
		rTemp.close();

		// Merge data
		const std::vector<int> &currentKeyPressCounts = retrieve_values();

		// NOTE: keyPressCounts from KeyLogger shall not be modified hereon until keylogOpMode = 0
		std::vector<int> mergedKeyPressCounts(numOfKeyCodes+1);
		for (int i = 0; i < currentKeyPressCounts.size(); ++i)
		{
			mergedKeyPressCounts[i] = currentKeyPressCounts[i] + loadedKeyPressCounts[i];
			// write_keypress_count alternative:
			// rwTemp << i << "=" << currentKeyPressCounts[i] + loadedKeyPressCounts[i] << "\n";
		}
		// Add loaded data to active numbering
		add_values_sync(loadedKeyPressCounts);

		// Write merged data
		std::ofstream wTemp("tempFile.txt");
		write_logger_times<std::ofstream>(wTemp, currentEpoch);
		write_keypress_count<std::ofstream>(wTemp, mergedKeyPressCounts);
		wTemp.close();
	}
	// If been more than 30 mins since last save, save to temp file
	else if (currentEpoch + (hourInMs/2) > lastTempSaveEpoch)
	{
		std::cout << "sync_temp_file: saving tempFile\n";
		// std::cout << "performing regular tempFile save\n";

		// NOTE: Important to ensure keylogger switches variables so that retrieve_values and merging is accurate
		keylogOpMode = 2;

		// NOTE: keyPressCounts from KeyLogger shall not be modified hereon until keylogOpMode = 0
		const std::vector<int> &currentKeyPressCounts = retrieve_values();

		std::ofstream writeTemp("tempFile.txt");
		write_logger_times<std::ofstream>(writeTemp, currentEpoch);
		write_keypress_count<std::ofstream>(writeTemp, currentKeyPressCounts);
		writeTemp.close();
	}

	keylogOpMode = 1;
}

void save_persistent_file()
{
	std::cout << "save_persistent_file\n";

	keylogOpMode = 2;
	savingPersistentFile = true;

	// NOTE: keyPressCounts from KeyLogger shall not be modified hereon until keylogOpMode = 0
	const std::vector<int> &currentKeyPressCounts = retrieve_values();

	// Save
	std::ofstream wSave("saveFile " + current_date_time() + ".txt");
	write_keypress_count<std::ofstream>(wSave, currentKeyPressCounts);
	wSave.close();

	// Reset keylogger state to zero and Overwrite tempFile to blank
	std::ofstream wTemp("tempFile.txt");
	wTemp.close();

	reset_values();

	keylogOpMode = 1;
	savingPersistentFile = false;
}

// Handle running temp sync and daily save on interval
void timer_save_and_sync()
{
	long long epochMsLastSave = get_ms_since_epoch().count();
	while (true)
	{
		// Sync temp file every half hour
		Sleep(hourInMs/2);
		sync_temp_file();

		long long epochMsCurrent = get_ms_since_epoch().count();
		if (epochMsCurrent > epochMsLastSave + dayInMs) // Creates new file every 24 hours
		{
			save_persistent_file();
			epochMsLastSave = epochMsCurrent;
		}
	}
}

int main()
{
	activeInitEpoch = get_ms_since_epoch().count();

	std::thread keyloggerThread(key_logger);

	// Load initially
	sync_temp_file();

	std::thread saveTimersThread(timer_save_and_sync);

	// FIXME: Ugly. Either hide the window, or put a mutex on console read/writing or whatever, and create a loop to
	// prompt the user for a specific input string to close
	while (keepRunning)
	{
		Sleep(1000);
		// system("pause");
		std::string userInput;
		std::cin >> userInput;
		if (userInput == ":quit")
		{
			keepRunning = false;
			std::cout << "lol\n";
			// Need to make sure these threads close, but they are endless, but I cba fixing it
			// keyloggerThread.join();
			// saveTimersThread.join();
			return 0;
		}
		else if (userInput == ":writeTemp")
		{
			sync_temp_file();
		}
		else if (userInput == ":saveDay")
		{
			save_persistent_file();
		}
		else if (userInput == ":help")
		{
			std::cout << "\n";
			std::cout << ":quit		- Quit\n";
			std::cout << ":writeTemp	- Save current session to tempFile\n";
			std::cout << ":saveDay	- Save current session to a new persistent day file\n";
			std::cout << "\n";
		}
		else
		{
			std::cout << "\n";
			std::cout << ":help to show help\n";
			std::cout << "\n";
		}
	}
}
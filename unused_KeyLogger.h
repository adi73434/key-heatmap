#pragma once

#include <vector>
#include <string>

class KeyLogger
{
public:
	KeyLogger()
	{
	}

	~KeyLogger();
	
	static void log_key(int key);
	void retrive_values();
	void wipe_values();
	void key_logger();
	int loggedQ = 0;
	int loggedW = 0;
	int loggedE = 0;
	int loggedR = 0;
	int loggedT = 0;
	int loggedY = 0;
	int loggedU = 0;
	int loggedI = 0;
	int loggedO = 0;
	int loggedP = 0;
	int loggedA = 0;
	int loggedS = 0;
	int loggedD = 0;
	int loggedF = 0;
	int loggedG = 0;
	int loggedH = 0;
	int loggedJ = 0;
	int loggedK = 0;
	int loggedL = 0;
	int loggedZ = 0;
	int loggedX = 0;
	int loggedC = 0;
	int loggedV = 0;
	int loggedB = 0;
	int loggedN = 0;
	int loggedM = 0;

private:
	std::string bufferWhileSaving;
};
#pragma once

extern bool keepRunning;

// 0 = Not saving/loading; 1 = Was saving/loading; 2 = Currently saving/loading;
// NOTE: keylogOpMode should probably be mutex locked, but:
// the tempFile and saveFile never overlap in execution and key_logger only changes the variable
// from 1 -> 0 when the overflow is being handled, during which keylogOpMode shouldn't be getting modified elsewhere
// as writing from the overflow would take a couple milliseconds and the next time that keylogOpMode would be modified elswehere
// would be in about an hour or whenever the timer_save_and_sync interval dictates
extern int keylogOpMode;

extern long long activeInitEpoch;

extern long long lastTempSaveEpoch;
extern long long lastTempInitEpoch;

extern bool savingPersistentFile;

extern int numOfKeyCodes;